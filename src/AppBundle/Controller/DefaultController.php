<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Checkout;
use AppBundle\Form\Type\CheckoutType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        ]);
    }

    /**
     * @Route("/checkout", name="checkout")
     * @Method("POST")
     */
    public function checkoutAction(Request $request)
    {
        return $this->processForm(new Checkout(), $request);
    }

    private function processForm(Checkout $checkout, Request $request)
    {
        $data = array();
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
        }

        if (!$this->authenticate($request)) {
            $response = new Response(json_encode(array(
                'result' => 'REJECT',
                'resultCode' => 401,
                'resultMessage' => 'Unauthorized',
            )), 401);
            return $response;
        }

        $form = $this->createForm(CheckoutType::class, $checkout);
        $form->submit($data);

        if ($form->isValid()) {
            if ($checkout->getAmount() <= 100) {
                // {'result':'OK','resultCode':1,'id':123}
                $response = new Response(json_encode(array(
                    'result' => 'OK',
                    'resultCode' => 1,
                    'id' => 123
                )));
            } else {
                // {'result':'DECLINE','resultCode':555,'resultMessage':'Amount exceed'}
                $response = new Response(json_encode(array(
                    'result' => 'DECLINE',
                    'resultCode' => 555,
                    'resultMessage' => 'Amount exceed'
                )));
            }
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }

        $response = new Response(json_encode(array(
            'result' => 'REJECT',
            'resultCode' => 400,
            'resultMessage' => 'Invalid input',
            // For debugging only:
            'rawInput' => $request->getContent(),
            'jsonInput' => $data,
            'details' => (string)$form->getErrors(true)
        )), 400);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Dummy authorization
     * This should support a one way token authorization
     * comparing the token provided with a customer key stored in the db.
     * For the sake of brevity, I only compare it with a hardcoded value.
     * @param Request $request
     */
    private function authenticate(Request $request)
    {
        $header = $request->headers->get('Authorization');
        $parts = explode(' ', $header);
        if (!$parts || $parts[0] != 'Bearer') {
            return false;
        }
        $auth = base64_decode($parts[1]);
        return ($auth == 'Secret123');
    }
}
