<?php
/**
 * Created by PhpStorm.
 * User: vezir
 * Date: 2/21/16
 * Time: 8:07 PM
 */

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Checkout
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="alpha",
     * )
     */
    protected $firstName = null;
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="alpha",
     * )
     */
    protected $lastName = null;
    /**
     * @var float
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="numeric",
     * )
     */
    protected $amount = null;
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Currency()
     */
    protected $currency = null;
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\CardScheme(
     *     schemes={"VISA", "MAESTRO", "MASTERCARD"},
     *     message="Unsupported card type or invalid card number (Visa, Maestro, Mastercard only). Try: 4242424242424242 or 5555555555554444."
     * )
     */
    protected $creditCardNumber = null;
    /**
     * @var int
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="int",
     * )
     * @Assert\Length(
     *      min = 3,
     *      max = 3
     * )
     */
    protected $cvv = null;
    /**
     * @var int
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="int",
     *     message="This value should be a number (without leading zero)."
     * )
     * @Assert\Range(
     *      min = 1,
     *      max = 12
     * )
     */
    protected $expMonth = null;
    /**
     * @var int
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="int",
     *     message="This value should be a number (without leading zero)."
     * )
     * @Assert\Range(
     *      min = 16,
     *      max = 99
     * )
     */
    protected $expYear = null;

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return Checkout
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return Checkout
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return Checkout
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return Checkout
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreditCardNumber()
    {
        return $this->creditCardNumber;
    }

    /**
     * @param string $cardNumber
     * @return Checkout
     */
    public function setCreditCardNumber($cardNumber)
    {
        $this->creditCardNumber = $cardNumber;
        return $this;
    }

    /**
     * @return int
     */
    public function getCvv()
    {
        return $this->cvv;
    }

    /**
     * @param int $cvv
     * @return Checkout
     */
    public function setCvv($cvv)
    {
        $this->cvv = $cvv;
        return $this;
    }

    /**
     * @return int
     */
    public function getExpMonth()
    {
        return $this->expMonth;
    }

    /**
     * @param int $expMonth
     * @return Checkout
     */
    public function setExpMonth($expMonth)
    {
        $this->expMonth = $expMonth;
        return $this;
    }

    /**
     * @return int
     */
    public function getExpYear()
    {
        return $this->expYear;
    }

    /**
     * @param int $expYear
     * @return Checkout
     */
    public function setExpYear($expYear)
    {
        $this->expYear = $expYear;
        return $this;
    }

}