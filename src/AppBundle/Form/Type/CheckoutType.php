<?php
/**
 * Created by PhpStorm.
 * User: vezir
 * Date: 2/21/16
 * Time: 8:17 PM
 */

namespace AppBundle\Form\Type;


use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CheckoutType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // FirstName, LastName, Amount, Currency, CreditCardNumber, Cvv, ExpMonth, ExpYear
        $builder
            ->add('FirstName', TextType::class)
            ->add('LastName', TextType::class)
            ->add('Amount', MoneyType::class)
            ->add('Currency', TextType::class)
            ->add('CreditCardNumber', TextType::class)
            ->add('Cvv', IntegerType::class)
            ->add('ExpMonth', IntegerType::class)
            ->add('ExpYear', IntegerType::class)
            ->add('send', SubmitType::class)
            ->setMethod('POST')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Checkout',
            'csrf_protection' => false,
        ));
    }
}